#include <stdio.h>
#include <stdlib.h>

void QuickSort(int *vet, int esq, int dir);

int main(int argc, char **argv)
{
  int i, n = 0;

  printf("Enter no. of elements: ");
  scanf(" %d", &n);

  int vet[n];
  int possibilidades = n * 5;

  for (i = 0; i < n; i++)
    vet[i] = rand() % possibilidades + 1;

  printf("Elements: ");
  for (int j = 0; j < n; j++)
    printf("%d  ", vet[j]);

  QuickSort(vet, 0, 10 - 1);

  printf("\nValores ordenados: ");

  for (int k = 0; k < n; k++)
  {
    printf("%d ", vet[k]);
  }
  return 0;
}

void QuickSort(int *vet, int esq, int dir)
{
  int i = esq;
  int j = dir;
  int meio = vet[(esq + dir) / 2];
  int y;

  while (i <= j)
  {
    while (vet[i] < meio && i < dir)
    {
      i++;
    }

    while (vet[j] > meio && j > esq)
    {
      j--;
    }

    if (i <= j)
    {
      y = vet[i];
      vet[i] = vet[j];
      vet[j] = y;
      i++;
      j--;
    }
  }

  if (j > esq)
  {
    QuickSort(vet, esq, j);
  }

  if (i < dir)
  {
    QuickSort(vet, i, dir);
  }
}