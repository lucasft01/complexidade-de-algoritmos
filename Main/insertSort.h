#include <stdio.h>
#include <stdlib.h>

void insertSort(int vet[], int tam)
{
  int j, val;

  for (int i = 1; i < tam; i++)
  {
    val = vet[i];

    for (j = i - 1; j >= 0 && vet[j] > val; j--)
    {
      vet[j + 1] = vet[j];
    }
    vet[j + 1] = val;
  }
}