#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "insertSort.h"
#include "quickSort.h"
#include "mergeSort.h"

int main()
{
	int n = 50000;
	int i, last, temp;

	printf("Calcula tempo para ordenar 10k, 20k, 30k, 40k e 50k: \n");

	int vet[n];
	int quick[n];
	int insert[n];
	int merge[n];
	int possibilidades = n * 5;
	for (int i = 0; i < n; i++)
	{
		int randomNumber = rand() % possibilidades + 1;
		vet[i] = randomNumber;
		quick[i] = randomNumber;
		insert[i] = randomNumber;
		merge[i] = randomNumber;
	}
	//Insert//
	clock_t inicioInsert10k;
	inicioInsert10k = clock();
	insertSort(insert, 10000);
	clock_t finalInsert10k;
	finalInsert10k = clock();
	clock_t tempoInsert10k = finalInsert10k - inicioInsert10k;

	clock_t inicioInsert20k;
	inicioInsert20k = clock();
	insertSort(insert, 20000);
	clock_t finalInsert20k;
	finalInsert20k = clock();
	clock_t tempoInsert20k = finalInsert20k - inicioInsert20k;

	clock_t inicioInsert30k;
	inicioInsert30k = clock();
	insertSort(insert, 30000);
	clock_t finalInsert30k;
	finalInsert30k = clock();
	clock_t tempoInsert30k = finalInsert30k - inicioInsert30k;

	clock_t inicioInsert40k;
	inicioInsert40k = clock();
	insertSort(insert, 40000);
	clock_t finalInsert40k;
	finalInsert40k = clock();
	clock_t tempoInsert40k = finalInsert40k - inicioInsert40k;

	clock_t inicioInsert50k;
	inicioInsert50k = clock();
	insertSort(insert, 50000);
	clock_t finalInsert50k;
	finalInsert50k = clock();
	clock_t tempoInsert50k = finalInsert50k - inicioInsert50k;

	//QUICK//
	clock_t inicioQuick10k;
	inicioQuick10k = clock();
	quickSort(quick, 0, 10000 - 1);
	clock_t finalQuick10k;
	finalQuick10k = clock();
	clock_t tempoQuick10k = finalQuick10k - inicioQuick10k;

	clock_t inicioQuick20k;
	inicioQuick20k = clock();
	quickSort(quick, 0, 20000 - 1);
	clock_t finalQuick20k;
	finalQuick20k = clock();
	clock_t tempoQuick20k = finalQuick20k - inicioQuick20k;

	clock_t inicioQuick30k;
	inicioQuick30k = clock();
	quickSort(quick, 0, 30000 - 1);
	clock_t finalQuick30k;
	finalQuick30k = clock();
	clock_t tempoQuick30k = finalQuick30k - inicioQuick30k;

	clock_t inicioQuick40k;
	inicioQuick40k = clock();
	quickSort(quick, 0, 40000 - 1);
	clock_t finalQuick40k;
	finalQuick40k = clock();
	clock_t tempoQuick40k = finalQuick40k - inicioQuick40k;

	clock_t inicioQuick50k;
	inicioQuick50k = clock();
	quickSort(quick, 0, 50000 - 1);
	clock_t finalQuick50k;
	finalQuick50k = clock();
	clock_t tempoQuick50k = finalQuick50k - inicioQuick50k;

	//MERGE
	clock_t inicioMerge10k;
	inicioMerge10k = clock();
	mergeSort(merge, 0, 10000 - 1);
	clock_t finalMerge10k;
	finalMerge10k = clock();
	clock_t tempoMerge10k = finalMerge10k - inicioMerge10k;

	clock_t inicioMerge20k;
	inicioMerge20k = clock();
	mergeSort(merge, 0, 20000 - 1);
	clock_t finalMerge20k;
	finalMerge20k = clock();
	clock_t tempoMerge20k = finalMerge20k - inicioMerge20k;

	clock_t inicioMerge30k;
	inicioMerge30k = clock();
	mergeSort(merge, 0, 30000 - 1);
	clock_t finalMerge30k;
	finalMerge30k = clock();
	clock_t tempoMerge30k = finalMerge30k - inicioMerge30k;

	clock_t inicioMerge40k;
	inicioMerge40k = clock();
	mergeSort(merge, 0, 40000 - 1);
	clock_t finalMerge40k;
	finalMerge40k = clock();
	clock_t tempoMerge40k = finalMerge40k - inicioMerge40k;

	clock_t inicioMerge50k;
	inicioMerge50k = clock();
	mergeSort(merge, 0, 50000 - 1);
	clock_t finalMerge50k;
	finalMerge50k = clock();
	clock_t tempoMerge50k = finalMerge50k - inicioMerge50k;

	//Insert
	FILE *fp;
	fp = fopen("./dados/Insert.dat", "w+");
	fprintf(fp, "#Insert\n");
	fprintf(fp, "#Tamanho\t\t\t\tTempo\n");
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 10000, (tempoInsert10k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 20000, (tempoInsert20k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 30000, (tempoInsert30k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 40000, (tempoInsert40k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 50000, (tempoInsert50k / (double)CLOCKS_PER_SEC) * 1000000);
	fclose(fp);

	fp = fopen("./dados/Merge.dat", "w+");
	fprintf(fp, "#Merge\n");
	fprintf(fp, "#Tamanho\t\t\t\tTempo\n");
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 10000, (tempoMerge10k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 20000, (tempoMerge20k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 30000, (tempoMerge30k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 40000, (tempoMerge40k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 50000, (tempoMerge50k / (double)CLOCKS_PER_SEC) * 1000000);
	fclose(fp);

	fp = fopen("./dados/Quick.dat", "w+");
	fprintf(fp, "#Quick\n");
	fprintf(fp, "#Tamanho\t\t\t\tTempo\n");
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 10000, (tempoQuick10k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 20000, (tempoQuick20k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 30000, (tempoQuick30k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 40000, (tempoQuick40k / (double)CLOCKS_PER_SEC) * 1000000);
	fprintf(fp, "%d\t\t\t\t\t\t%f\n", 50000, (tempoQuick50k / (double)CLOCKS_PER_SEC) * 1000000);
	fclose(fp);

	printf("\n----Tempos----");
	printf("\nInsertSort:");
	printf("\nTempo para 10k: %f milissegundos", (tempoInsert10k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 20k: %f milissegundos", (tempoInsert20k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 30k: %f milissegundos", (tempoInsert30k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 40k: %f milissegundos", (tempoInsert40k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 50k: %f milissegundos", (tempoInsert50k / (double)CLOCKS_PER_SEC) * 1000000);

	printf("\n\nMergeSort:");
	printf("\nTempo para 10k: %f milissegundos", (tempoMerge10k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 20k: %f milissegundos", (tempoMerge20k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 30k: %f milissegundos", (tempoMerge30k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 40k: %f milissegundos", (tempoMerge40k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 50k: %f milissegundos", (tempoMerge50k / (double)CLOCKS_PER_SEC) * 1000000);

	printf("\n\nQuickSort:");
	printf("\nTempo para 10k: %f milissegundos", (tempoQuick10k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 20k: %f milissegundos", (tempoQuick20k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 30k: %f milissegundos", (tempoQuick30k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 40k: %f milissegundos", (tempoQuick40k / (double)CLOCKS_PER_SEC) * 1000000);
	printf("\nTempo para 50k: %f milissegundos", (tempoQuick50k / (double)CLOCKS_PER_SEC) * 1000000);

	fprintf(popen("gnuplot -persistent", "w"), "%s", "./dados/Quick.dat");

	return 0;
}