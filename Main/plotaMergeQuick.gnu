# -------------------------------------------------------------------------------------------------------
# Este programa GnuPlot gera uma janela grafica lê e apresenta varios arquivos de dados.
#
# Autor		: Lucas Fagundes Teixeira
# Ano		: 2019
# Semestre	: 1
# Disciplina: Análise e Complexidade de Algortimos
#
# -------------------------------------------------------------------------------------------------------
set title "Gráfico para Apresentação do Tempo de Execução dos Algoritmos InsertSort, MergeSort e QuickSort"

# Estabelece o tamanho da janela automaticamente de acordo com os dados
set autoscale
set xlabel "Número de Entradas"
set ylabel "Tempo de Execução"

# Estabelece a posicao, em coordenadas dos dados, onde os títulos irão ser apresentados
set key left top Left title "Legenda" box

#Estabelece linhas de grade
set grid

# Lê os arquivos e gera o gráfico

plot \
        "./dados/Merge.dat" title "MergeSort" w linespoints, "./dados/Quick.dat" title "QuickSort" w linespoints

pause -1 "Pressione enter para sair e terminar as aplicacoes..."
