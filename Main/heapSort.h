#include <stdio.h>
#include <stdlib.h>

void create(int[]);
void down_adjust(int[], int);

void heapSort(int heap[], int n)
{
	int last, temp; // 3

	//create a heap
	heap[0] = n;	//1
	create(heap); //1

	//sorting
	while (heap[0] > 1) //n+1
	{
		//swap heap[1] and heap[last]
		last = heap[0];				//n
		temp = heap[1];				//n
		heap[1] = heap[last]; //n
		heap[last] = temp;		//n
		heap[0]--;						//n
		down_adjust(heap, 1); //n*
	}
	for (int k = 0; k < n; k++)
		printf("%d ", heap[k]);
}

void create(int heap[])
{
	int i, n;		 // 1
	n = heap[0]; //1

	for (i = n / 2; i >= 1; i--) //n+1
		down_adjust(heap, i);			 //n*
}

void down_adjust(int heap[], int i)
{
	int j, temp, n, flag = 1; //1
	n = heap[0];							//1

	while (2 * i <= n && flag == 1) //n² + n
	{
		j = 2 * i;															 //j points to left child
		if (j + 1 <= n && heap[j + 1] > heap[j]) //n² + n
			j = j + 1;
		if (heap[i] > heap[j]) //n² + n
			flag = 0;
		else // n² + n
		{
			temp = heap[i];
			heap[i] = heap[j];
			heap[j] = temp;
			i = j;
		}
	}
}

//Complexidade:
// 10n² + 15n + 13 == O(n²)