#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "heapSort.h"
#include "insertSort.h"
#include "quickSort.h"
#include "mergeSort.h"

int main()
{
	int n, i, last, temp;

	printf("Entre no. de elementos: ");
	scanf("%d", &n);

	int vet[n];
	int quick[n];
	int insert[n];
	int merge[n];
	int heap[n];
	int possibilidades = n * 5;
	for (int i = 0; i < n; i++)
	{
		int randomNumber = rand() % possibilidades + 1;
		vet[i] = randomNumber;
		quick[i] = randomNumber;
		insert[i] = randomNumber;
		merge[i] = randomNumber;
		heap[i] = randomNumber;
	}

	clock_t inicioInsert;
	inicioInsert = clock();
	insertSort(insert, n);
	clock_t finalInsert;
	finalInsert = clock();
	clock_t tempoInsert = finalInsert - inicioInsert;

	clock_t inicioQuick;
	inicioQuick = clock();
	quickSort(quick, 0, n - 1);
	clock_t finalQuick;
	finalQuick = clock();
	clock_t tempoQuick = finalQuick - inicioQuick;

	clock_t inicioMerge;
	inicioMerge = clock();
	mergeSort(merge, 0, n - 1);
	clock_t finalMerge;
	finalMerge = clock();
	clock_t tempoMerge = finalMerge - inicioMerge;

	clock_t inicioHeap;
	inicioHeap = clock();
	heapSort(heap, n);
	clock_t finalHeap;
	finalHeap = clock();
	clock_t tempoHeap = finalHeap - inicioHeap;

	if (n <= 20)
	{
		printf("Elements: ");
		for (int j = 0; j < n; j++)
			printf("%d  ", vet[j]);

		printf("\n\n----Prova e Tempo----");

		printf("\nInsertSort:\n");
		for (int l = 0; l < n; l++)
			printf("%d ", insert[l]);
		printf("\nTempo:%f segundos", (tempoInsert) / (double)CLOCKS_PER_SEC);

		printf("\n\nQuickSort:\n");
		for (int k = 0; k < n; k++)
			printf("%d ", quick[k]);
		printf("\nTempo:%f segundos", (tempoQuick) / (double)CLOCKS_PER_SEC);

		printf("\n\nMergeSort:\n");
		for (int m = 0; m < n; m++)
			printf("%d ", merge[m]);
		printf("\nTempo:%f segundos", (tempoMerge) / (double)CLOCKS_PER_SEC);

		// printf("\n\nHeapSort:\n");
		// for (int indice = 0; indice < n; indice++)
		// 	printf("%d ", heap[indice]);
		// printf("\nTempo:%f segundos", (tempoHeap) / (double)CLOCKS_PER_SEC);
	}
	else
	{
		printf("\n----Tempos----");

		printf("\nInsertSort:");
		printf("\nTempo:%f segundos", (tempoInsert) / (double)CLOCKS_PER_SEC);

		printf("\n\nQuickSort:");
		printf("\nTempo:%f segundos", (tempoQuick) / (double)CLOCKS_PER_SEC);

		printf("\n\nMergeSort:");
		printf("\nTempo:%f segundos", (tempoMerge) / (double)CLOCKS_PER_SEC);

		printf("\n\nHeapSort:");
		printf("\nTempo:%f segundos", (tempoHeap) / (double)CLOCKS_PER_SEC);
	}

	return 0;
}