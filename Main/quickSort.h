#include <stdio.h>
#include <stdlib.h>

void quickSort(int *vet, int esq, int dir)
{
  int i = esq;
  int j = dir;
  int meio = vet[(esq + dir) / 2];
  int y;

  while (i <= j)
  {
    while (vet[i] < meio && i < dir)
    {
      i++;
    }

    while (vet[j] > meio && j > esq)
    {
      j--;
    }

    if (i <= j)
    {
      y = vet[i];
      vet[i] = vet[j];
      vet[j] = y;
      i++;
      j--;
    }
  }

  if (j > esq)
  {
    quickSort(vet, esq, j);
  }

  if (i < dir)
  {
    quickSort(vet, i, dir);
  }
}