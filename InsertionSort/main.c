#include <stdio.h>
#include <stdlib.h>

void sort(int *vet, int tam);

int main()
{

  int n = 0;
  printf("Enter no. of elements: ");
  scanf(" %d", &n);

  int vet[n];
  int possibilidades = n * 5;

  for (int i = 0; i < n; i++)
    vet[i] = rand() % possibilidades + 1;

  printf("Elements: ");
  for (int j = 0; j < n; j++)
    printf("%d  ", vet[j]);

  sort(vet, n);

  printf("\nOrdenado: ");
  for (int i = 0; i < n; i++)
  {
    printf("%d ", vet[i]);
  }
}

void sort(int *vet, int tam)
{
  int j, val;

  for (int i = 1; i < tam; i++)
  {
    val = vet[i];

    for (j = i - 1; j >= 0 && vet[j] > val; j--)
    {
      vet[j + 1] = vet[j];
    }

    vet[j + 1] = val;
  }
}